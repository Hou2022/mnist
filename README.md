This is a introduction of each project folders.
1.	mnist classification
Perform multi-class classification using the MNIST dataset as the first step of the experiment.

2.	Normalizing flows with MNIST
This is someone else's project. I uploaded as a reference for the second step to generate mnist 
digit images using normalizing flow. 

3.	Nomalizing flow generate mnist
4.	Nomalizing flow geneate MNIST v.2
These are the code for generating MNIST digits images with normalizing flows. There are only two 
epochs in this project because this is just a test. After training the model, I tested to generate
50 images. The code was overwritten as I continued to use the code for the third step of the experiment.

5.	Normalizing flow generate noise
This is a failed attempt. I misunderstood what the teacher told me. I didn't train the model with 
my own generated Gaussian distributed array dataset.

6.	Real-NVP
7.	Realnvp V.2
These are the projects for the third step. Realnvp V.2 is the final code. I use the normalizing 
flow model with Real-NVP transformation to train the dataset, and test whether it is a Gaussian
distributed array based on the output normal distribution histogram. The difference between Project
6 and 7 differ is using different parameters to train model.
